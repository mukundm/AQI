# AQI
Air Quality

This project is based on Live data of the Air Quality Index and Weather at the user's present location

# Setup
 ` npm i && npm start `

# Overview

### Index
![Index](https://i.imgur.com/4BLF0jp.png)

### Map
![Map](https://i.imgur.com/HbVTCED.png)

### City-1
![City-1](https://i.imgur.com/GMJHpba.png)

### City-2
![City-2](https://i.imgur.com/ceQ4fux.png)

### Features
![Features](https://i.imgur.com/BCVyu7r.png)

### Team
![Team](https://i.imgur.com/Z5cB0ZX.png)
 


# Used :
* VS Code,
* Node.js,
* HTML,
* JS,
* Leaflet,
* API,
* NeDB(Local Database)
